Rails.application.routes.draw do
  root 'posts#index', as: 'home'

  get 'about' => 'pages#about'

  # resources :posts the following routes:
  #   => posts#index     (GET)
  #   => posts#create    (POST)
  #   => posts#new       (GET)
  #   => posts#edit      (GET)
  #   => posts#show      (GET)
  #   => posts#update    (PATCH)
  #   => posts#update    (PUT)
  #   => posts#destroy   (DELETE)
  resources :posts do
    resources :comments
  end
end
