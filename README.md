# A plain and simple blog

Uses Rails.

### Install Dependencies

`$ bundle install`

### Run Migrations

`$ rails db:migrate`

### Run Server

`$ rails s`

### Basic HTTP Auth Creds

```
username: basil
password: letmein
```