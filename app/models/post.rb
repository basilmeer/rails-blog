class Post < ApplicationRecord
    # Adding relationship with commments
    has_many :comments
    # Adding title validation
    validates :title, presence: true,
                        length: {minimum: 5}
    # Adding body validation
    validates :body,  presence: true,
                        length: {maximum: 300}
end
