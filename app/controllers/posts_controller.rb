class PostsController < ApplicationController
    http_basic_authenticate_with name: "basil", password: "letmein", except: [:index, :show]

    def index
        @posts = Post.all

    end

    def show
        @post = Post.find(params[:id])
    end

    def new
        @post = Post.new
    end

    def create
        # Pass the form data through an object of the Post model
        @post = Post.new(post_params)

        if(@post.save)
            # If the form validates and saves then redirect
            redirect_to @post
        else
            # If it doesn't then refresh the page
            render 'new'
        end
    end

    def edit
        @post = Post.find(params[:id])
    end

    def update
        @post = Post.find(params[:id])

        if(@post.update(post_params))
            redirect_to @post
        else
            render 'edit'
        end
    end

    def destroy
        Post.find(params[:id]).destroy
        
        redirect_to home_path
    end

    # Make sure that only title and body are passed through the form, and that they're required
    private def post_params
        params.require(:posts).permit(:title, :body)
    end
end
