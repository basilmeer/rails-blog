class CommentsController < ApplicationController
    http_basic_authenticate_with name: "basil", password: "letmein", only: [:destroy]

    def create
        @post = Post.find(params[:post_id])

        @comment = @post.comments.create(comment_params)
    
        redirect_to post_path(@post)
    end


    def destroy
        @post = Post.find(params[:id])
        Comment.find(params[:id]).destroy
        redirect_to post_path(@post)
    end

    private def comment_params
        params.require(:comments).permit(:username, :body)
    end
end